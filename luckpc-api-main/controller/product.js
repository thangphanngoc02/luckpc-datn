const ProductSerevice = require('../service/product');
const Product = require('../models/product');
const createProduct = async (req, res) => {
    try {
        const response = await ProductSerevice.createProduct(req.body);
        if (response)
            return res.status(200).json({
                success: true,
                response,
            });
    } catch (e) {
        console.log(e);
        return res.status(500).json({
            mes: e.mes,
        });
    }
};

const getProducts = async (req, res) => {
    try {
        const { name, page, category } = req.query;
        let limit = process.env.LIMIT;
        const options = {
            page,
            limit,
            category,
        };
        if (name) {
            options.name = name;
        }
        if (page) {
            options.page = page;
        }
        if (category) {
            options.category = category;
        }
        const response = await ProductSerevice.getProducts({ ...options });
        if (response)
            return res.status(200).json({
                success: true,
                products: response.product,
            });
    } catch (e) {
        return res.status(500).json({
            mes: e.mes,
        });
    }
};
const getProduct = async (req, res) => {
    try {
        const { id } = req.params;
        const response = await ProductSerevice.getProduct(id);
        if (response)
            return res.status(200).json({
                success: true,
                product: response.product,
            });
    } catch (e) {
        return res.status(500).json({
            mes: e.mes,
        });
    }
};

const deleteProduct = async (req, res) => {
    const { id } = req.params;
    const data = await Product.findOne({ id: id });
    if (!data) {
        return res.status(200).json({ message: 'Không tìm thấy sản phẩm !!!' });
    } else {
        await Product.deleteOne({ id: data.id });
        return res.status(200).json({ message: 'Đã xóa sản phẩm thành công !!!' });
    }
};
const updateProduct = async (req, res) => {
    // try {
    //     const { id } = req.params;
    //     const response = await ProductSerevice.updateProduct(id, req.body);
    //     if (response)
    //         return res.status(200).json({
    //             success: true,
    //             product: response.product,
    //         });
    // } catch (e) {
    //     console.log(e);
    //     return res.status(500).json({
    //         mes: e.mes,
    //     });
    // }
    try {
        const { id } = req.params;
        const data = await Product.findOne({ id: id });

        // xử lý upadte ảnh !!!

        if (data) {
            data.updateOne({
                name: req.body.name || data.name,
                price: req.body.price || data.price,
                image: req.body.image || data.image,
                discount: req.body.discount || data.discount,
                des: req.body.des || data.des,
                description: req.body.description || data.description,
            }).then((newData) => newData);
            return res.status(200).json({ message: 'Sửa Thành Công !!!' });
        }
    } catch (error) {
        console.log(error);
    }
};

const createReviews = async (req, res) => {
    try {
        const { id } = req.params;
        const response = await ProductSerevice.createReviews(id, req.body, req.user.id);
        if (response.success) {
            return res.status(200).json({
                success: true,
                product: response.product,
            });
        } else {
            return res.status(400).json({
                success: false,
                message: response.message,
            });
        }
    } catch (e) {
        return res.status(500).json({
            success: false,
            message: e.message,
        });
    }
};

module.exports = {
    createProduct,
    getProducts,
    getProduct,
    deleteProduct,
    updateProduct,
    createReviews,
};
