const router = require('express').Router();
const Joi = require('joi');
const userController = require('../controller/user');
const validateDto = require('../middleware/validate');
const { stringReq, string } = require('../middleware/JoiSheme');
const { verifyToken, isAdmin, checkToken } = require('../middleware/auth');
router.post(
  '/register',
  validateDto(
    Joi.object({
      name: stringReq,
      email: stringReq,
      password: stringReq,
    }),
  ),
  userController.rerister,
);
router.post(
  '/',
  validateDto(
    Joi.object({
      name: stringReq,
      email: stringReq,
      password: stringReq,
      role: string,
      phone: string,
      address: string,
    }),
  ),
  verifyToken,
  isAdmin,
  userController.rerister,
);
router.post(
  '/login',
  validateDto(
    Joi.object({
      email: stringReq,
      password: stringReq,
    }),
  ),
  userController.login,
);
router.get('/get-user-token', verifyToken, userController.getUserToken);
router.get('/get-users', verifyToken, isAdmin, userController.getUsers);
router.get('/refesToken', checkToken, userController.refesToken);
router.put(
  '/update-user/:id',
  validateDto(
    Joi.object({
      name: stringReq,
      phone: stringReq,
      address: stringReq,
    }),
  ),
  verifyToken,
  userController.updateUser,
);

router.delete('/delete/:id', verifyToken, isAdmin, userController.deleteleteUser);
router.post('/restore/:id', verifyToken, isAdmin, userController.restoreUser);
router.patch('/add-card/:id', verifyToken, userController.addProductCart);
router.patch('/remove-card/:id', verifyToken, userController.removeProductCart);

router.put(
  '/:id',
  validateDto(
    Joi.object({
      name: stringReq,
      email: stringReq,
      phone: string,
      address: string,
      role: stringReq,
      password: string,
    }),
  ),
  verifyToken,
  isAdmin,
  userController.adminUpdateUser,
);
router.get('/:id', verifyToken, isAdmin, userController.getUser);

module.exports = router;
