import React, { useState } from 'react';

import DrawerCpn from '../../../common/Drawer/Drawer';

import styles from './User.module.scss';
import { useForm } from 'react-hook-form';
import * as UserApi from '../../../../../src/api/user';
import { toast } from 'react-toastify';

const AddDrawer = (props) => {
    const { children, refresh } = props;

    const [visible, setVisible] = useState(false);

    const onOpen = () => setVisible(true);
    const onClose = () => setVisible(false);

    const {
        register,
        handleSubmit,
        reset,
        formState: { errors },
    } = useForm();

    const onSubmit = async (data) => {
        try {
            await UserApi.addUser(data);
            toast.success('Thêm user thành công');
            reset();
            onClose();
            refresh();
        } catch (error) {
            toast.error(error?.response?.data?.mes);
            console.log(error);
        }
    };

    return (
        <>
            <span onClick={onOpen}>{children}</span>

            <DrawerCpn isOpen={visible} setisOpen={setVisible}>
                <div className={styles.form}>
                    <form action="" onSubmit={handleSubmit(onSubmit)}>
                        <h2 className={styles.title}>Add User</h2>

                        <div className={styles.formGroup}>
                            <label htmlFor="" className={styles.label}>
                                Name *
                            </label>
                            <input
                                type="text"
                                placeholder="Full name"
                                {...register('name', {
                                    required: 'Vui lòng nhập họ tên',
                                })}
                                className={styles.inputField}
                            />

                            {errors?.name?.message && <p className={styles.errorTxt}>{errors?.name?.message}</p>}
                        </div>

                        <div className={styles.formGroup}>
                            <label htmlFor="" className={styles.label}>
                                Email *
                            </label>
                            <input
                                type="email"
                                placeholder="Email"
                                {...register('email', {
                                    required: 'Vui lòng nhập email',
                                    pattern: {
                                        value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                        message: 'Email không đúng định dạng',
                                    },
                                })}
                                className={styles.inputField}
                            />

                            {errors?.email && <p className={styles.errorTxt}>{errors?.email?.message}</p>}
                        </div>

                        <div className={styles.formGroup}>
                            <label htmlFor="" className={styles.label}>
                                Password *
                            </label>
                            <input
                                type="password"
                                placeholder="Password"
                                {...register('password', {
                                    required: 'Vui lòng nhập mật khẩu',
                                })}
                                className={styles.inputField}
                            />

                            {errors?.password?.message && (
                                <p className={styles.errorTxt}>{errors?.password?.message}</p>
                            )}
                        </div>

                        <div className={styles.formGroup}>
                            <label htmlFor="" className={styles.label}>
                                Role *
                            </label>

                            <select
                                name=""
                                id=""
                                {...register('role', {
                                    required: 'Vui lòng chọn role',
                                })}
                                className={styles.inputField}
                            >
                                <option value="">Select role</option>
                                <option value="user">User</option>
                                <option value="Admin">Admin</option>
                            </select>

                            {errors?.role?.message && <p className={styles.errorTxt}>{errors?.role?.message}</p>}
                        </div>

                        <div className={styles.formGroup}>
                            <label htmlFor="" className={styles.label}>
                                Phone
                            </label>

                            <input
                                type="text"
                                placeholder="Phone number"
                                {...register('phone')}
                                className={styles.inputField}
                            />
                        </div>

                        <div className={styles.formGroup}>
                            <label htmlFor="" className={styles.label}>
                                Address
                            </label>

                            <input
                                type="text"
                                placeholder="Address"
                                {...register('address')}
                                className={styles.inputField}
                            />
                        </div>

                        <button type="submit" className={styles.btnAdd}>
                            Add user
                        </button>
                    </form>
                </div>
            </DrawerCpn>
        </>
    );
};

export default AddDrawer;
