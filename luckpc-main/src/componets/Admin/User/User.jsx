import React, { useEffect, useState } from 'react';
import Tabble from '../../common/Tabble/Tabble';
import * as UserApi from '../../../api/user.js';
import { AiOutlineDelete } from 'react-icons/ai';
import { FaPencilAlt } from 'react-icons/fa';
import Swal from 'sweetalert2';
import { CiCirclePlus } from 'react-icons/ci';
import AddDrawer from './AddDrawer';
import EditDrawer from './EditDrawer/index.jsx';
import moment from 'moment';
import { MdRestore } from 'react-icons/md';

function User() {
    const [dataTable, setDataTable] = useState([]);
    const [editDrawer, setEditDrawer] = useState({
        id: '',
        visible: false,
    });

    const getUsers = async () => {
        try {
            const res = await UserApi.getUsser();
            if (res.success) {
                const data = res?.users?.map((el, index) => {
                    return {
                        id: el?._id,
                        Stt: index + 1,
                        name: el?.name,
                        email: el?.email,
                        phone: el?.phone,
                        address: el?.address,
                        role: el?.role,
                        deletedAt: el?.deletedAt,
                    };
                });

                setDataTable(data);
            }
        } catch (err) {
            console.log(err);
        }
    };

    const onEdit = (id) => {
        setEditDrawer({ id, visible: true });
    };

    const columns = [
        {
            Header: 'STT',
            accessor: 'Stt',
        },
        {
            Header: 'ID',
            accessor: 'id',
        },
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'Email',
            accessor: 'email',
        },
        {
            Header: 'Phone',
            accessor: 'phone',
        },
        {
            Header: 'Address',
            accessor: 'address',
        },
        {
            Header: 'Role',
            accessor: 'role',
        },
        {
            Header: 'Deleted At',
            accessor: 'deletedAt',
            Cell: ({ value }) => {
                return <p>{value && moment(value).format('DD/MM/YYYY HH:mm:ss')}</p>;
            },
        },
        {
            Header: 'Actions',
            Cell: ({ row }) => {
                const isDeleted = row.values.deletedAt;

                return (
                    <div style={{ display: 'flex' }}>
                        {row.values.role.toLowerCase() !== 'admin' && (
                            <span
                                onClick={() => handleDelete(row)}
                                style={{
                                    padding: '8px',
                                    border: '1px black solid',
                                    borderRadius: '4px',
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignContent: 'center',
                                    marginRight: '2px',
                                    color: 'red',
                                    cursor: 'pointer',
                                }}
                            >
                                <AiOutlineDelete />
                            </span>
                        )}

                        {!isDeleted ? (
                            <span
                                onClick={() => onEdit(row.values.id)}
                                style={{
                                    padding: '8px',
                                    border: '1px black solid',
                                    borderRadius: '4px',
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignContent: 'center',
                                    color: 'green',
                                    cursor: 'pointer',
                                }}
                            >
                                <FaPencilAlt />
                            </span>
                        ) : (
                            <span
                                onClick={() => handleRestore(row)}
                                style={{
                                    padding: '8px',
                                    border: '1px black solid',
                                    borderRadius: '4px',
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignContent: 'center',
                                    color: 'green',
                                    cursor: 'pointer',
                                }}
                            >
                                <MdRestore />
                            </span>
                        )}
                    </div>
                );
            },
        },
    ];

    const handleDelete = async (data) => {
        const isDeleted = data?.values?.deletedAt;
        try {
            const { id } = data?.values;
            Swal.fire({
                title: isDeleted
                    ? 'Bạn chắc chắn muốn xoá người dùng này khỏi hệ thống?'
                    : 'Bạn có muốn xóa người dùng này?',
                showCancelButton: true,
                confirmButtonText: 'Xóa',
            }).then(async (result) => {
                if (result.isConfirmed) {
                    const res = await UserApi.deleteUser(id);
                    if (res.success) {
                        Swal.fire('Đã xóa!', '', 'Thành công');
                        getUsers();
                    }
                }
            });
        } catch (err) {}
    };

    const handleRestore = async (data) => {
        try {
            const { id } = data?.values;
            Swal.fire({
                title: 'Bạn có muốn khôi phục người dùng này?',
                showCancelButton: true,
                confirmButtonText: 'Khôi phục',
            }).then(async (result) => {
                if (result.isConfirmed) {
                    const res = await UserApi.restoreUser(id);
                    if (res.success) {
                        Swal.fire('Đã khôi phục!', '', 'Thành công');
                        getUsers();
                    }
                }
            });
        } catch (err) {}
    };

    useEffect(() => {
        getUsers();
    }, []);

    return (
        <>
            <div className="user">
                <AddDrawer refresh={getUsers}>
                    <div className="product-admin--create">
                        <div className="product-admin--create--btn">
                            <CiCirclePlus size={24} />
                            <p>Tạo mới</p>
                        </div>
                    </div>
                </AddDrawer>

                <Tabble title="Tất cả người dùng" data={dataTable} columns={columns} />
            </div>

            <EditDrawer
                userId={editDrawer.id}
                visible={editDrawer.visible}
                setVisible={(visible) => {
                    setEditDrawer((prev) => ({ ...prev, visible }));
                }}
                refresh={getUsers}
            />
        </>
    );
}

export default User;
